local methods = {}

local function checkprops (node, props)
  for k,v in pairs(props) do
    if node[k] ~= v then
      return false
    end
  end
  return true
end

function methods:find (name, props)
  for i,node in ipairs (self) do
    if type(node) == 'table' and node[0] == name then
      if not props or checkprops(node, props) then
        return node
      end
    end
  end
  return nil
end

local metatable = { __index = methods }

local xml = {}

local function makenode(label, xargs)
  node = {[0] = label}
  string.gsub(xargs, "([%w:-]+)=([\"'])(.-)%2", function (w, _, a)
    node[w] = a
  end)
  setmetatable (node, metatable)
  return node
end

local function dumpattrs(node)
  local l = {}
  for k,v in pairs(node) do
    if type(k) ~= 'number' then
      l[#l + 1] = tostring(k) .. ' = ' .. tostring(v)
    end
  end
  return table.concat (l, ', ')
end

local function dumplabels(stack)
  local l = {}
  for i,v in ipairs(stack) do
    if v[0] then l[#l + 1] = v[0] .. "[" .. dumpattrs(v) .. "]" end
  end
  return table.concat (l, ' > ')
end

function xml.parse(s)
  local stack = {}
  local top = makenode(nil, "")
  table.insert(stack, top)
  local ni,c,label,xarg,empty
  local i, j = 1, 1
  while true do
    ni,j,c,comment,pi = string.find(s, "<(%/?)(!?)(%??)", i)
    if not ni then break end
    local text = string.sub(s, i, ni-1)
    if not string.find(text, "^%s*$") then
      table.insert(top, text)
    end
    i = j + 1
    if comment == '!' then
      ni,j = string.find(s, "^%-%-.-%-%->", i)
      i = j + 1
    else
      ni,j,label,xarg,empty,epi = string.find(s, "^([%w?:-]+)(.-)(%/?)(%??)>", i)
      if empty == "/" or (pi == '?' and pi == epi) then  -- empty element tag or PI element
        table.insert(top, makenode(pi..label, xarg))
      elseif epi ~= "" then
        error ("?> can only finish a PI node, not: "..label)
      elseif c == "" then   -- start tag
        top = makenode(label, xarg)
        table.insert(stack, top)   -- new level
      else  -- end tag
        local toclose = table.remove(stack)  -- remove top
        top = stack[#stack]
        if #stack < 1 then
          error("nothing to close with "..label)
        end
        if toclose[0] ~= label then
          error("trying to close "..toclose[0].."["..dumpattrs(toclose).."] with "..label.."\n  stack: "..dumplabels(stack))
        end
        table.insert(top, toclose)
      end
      i = j + 1
    end
  end
  local text = string.sub(s, i)
  if not string.find(text, "^%s*$") then
    table.insert(stack[#stack], text)
  end
  if #stack > 1 then
    error("unclosed "..stack[#stack].label)
  end
  return stack[1]
end

return xml
