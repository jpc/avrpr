lfs = require 'lfs'
xml = require 'xml'

local function find(node, ...)
  for i,s in ipairs{...} do
    local label = s[1]
    s[1] = nil
    nnode = node:find(label, s)
    if not nnode then
      --error ("could not find child node " .. label .. " of node " .. node[0])
      return nil
    end
    node = nnode
  end
  return node
end

local function getsignature (device)
  local signatures = find (device, {'property-groups'}, {'property-group', name='SIGNATURES'})
  --local signatures = device:find'property-groups':find('property-group', {name='SIGNATURES'})
  local s = {}
  for i=0,2 do
    s[i+1] = string.format ("%02x", tonumber (signatures:find('property', {name='SIGNATURE'..i}).value))
  end
  return table.concat (s, ' ')
end

local function getmem (device, addrspace, segment)
  local mem = find (device, {'address-spaces'}, {'address-space', name = addrspace}, {'memory-segment', name = segment})
  if not mem then
    return { size = 0, pagesize = 0 }
  else
    return {
      size = tonumber (mem.size),
      pagesize = tonumber (mem.pagesize)
    }
  end
end

local function parsedev (fname)
  local text = io.open(fname):read('*all')
  tree = xml.parse(text)
  device = tree:find'avr-tools-device-file':find'devices':find'device'
  if device.architecture ~= 'AVR8' and device.architecture ~= 'AVR8L' then return nil end
  return {
    name = device.name,
    signature = getsignature (device),
    flash = getmem (device, 'prog', 'FLASH'),
    eeprom = getmem (device, 'eeprom', 'EEPROM'),
  }
end

local function q (s)
  return string.format ("%q", s)
end

local function dump (t)
  if type(t) == 'table' then
    local s = {'{'}
    for k,v in pairs (t) do
      s[#s+1] = '[' .. q(k) .. '] = '
      s[#s+1] = dump (v)
      s[#s+1] = ','
    end
    s[#s] = '}'
    return table.concat(s)
  elseif type(t) == 'string' then
    return q(t)
  else
    return tostring(t)
  end
end

local function parseall (base)
  print ("local devices = {}")
  for name in lfs.dir(base) do
    if name:match ('.atdf$') then
      dev = parsedev (base .. name)
      if dev then
        print ('devices[' .. q(dev.name) .. '] = ' .. dump (dev))
        print ('devices[' .. q(dev.signature) ..'] = devices[' .. q(dev.name) ..']')
      end
    end
  end
  print ("return devices")
end

parseall('devices/')
