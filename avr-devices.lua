local devices = {}
devices["AT90CAN128"] = {["signature"] = "1e 97 81",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "AT90CAN128",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 81"] = devices["AT90CAN128"]
devices["AT90CAN32"] = {["signature"] = "1e 95 81",["eeprom"] = {["pagesize"] = 8,["size"] = 1024},["name"] = "AT90CAN32",["flash"] = {["pagesize"] = 256,["size"] = 32768}}
devices["1e 95 81"] = devices["AT90CAN32"]
devices["AT90CAN64"] = {["signature"] = "1e 96 81",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "AT90CAN64",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 81"] = devices["AT90CAN64"]
devices["AT90PWM1"] = {["signature"] = "1e 93 83",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM1",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 83"] = devices["AT90PWM1"]
devices["AT90PWM161"] = {["signature"] = "1e 94 8b",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM161",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 8b"] = devices["AT90PWM161"]
devices["AT90PWM216"] = {["signature"] = "1e 94 83",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM216",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 83"] = devices["AT90PWM216"]
devices["AT90PWM2B"] = {["signature"] = "1e 93 83",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM2B",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 83"] = devices["AT90PWM2B"]
devices["AT90PWM316"] = {["signature"] = "1e 94 83",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM316",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 83"] = devices["AT90PWM316"]
devices["AT90PWM3B"] = {["signature"] = "1e 93 83",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM3B",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 83"] = devices["AT90PWM3B"]
devices["AT90PWM81"] = {["signature"] = "1e 93 88",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90PWM81",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 88"] = devices["AT90PWM81"]
devices["AT90USB1286"] = {["signature"] = "1e 97 82",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "AT90USB1286",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 82"] = devices["AT90USB1286"]
devices["AT90USB1287"] = {["signature"] = "1e 97 82",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "AT90USB1287",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 82"] = devices["AT90USB1287"]
devices["AT90USB162"] = {["signature"] = "1e 94 82",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90USB162",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 82"] = devices["AT90USB162"]
devices["AT90USB646"] = {["signature"] = "1e 96 82",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "AT90USB646",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 82"] = devices["AT90USB646"]
devices["AT90USB647"] = {["signature"] = "1e 96 82",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "AT90USB647",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 82"] = devices["AT90USB647"]
devices["AT90USB82"] = {["signature"] = "1e 93 82",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "AT90USB82",["flash"] = {["pagesize"] = 128,["size"] = 8192}}
devices["1e 93 82"] = devices["AT90USB82"]
devices["ATmega128"] = {["signature"] = "1e 97 02",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega128",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 02"] = devices["ATmega128"]
devices["ATmega1280"] = {["signature"] = "1e 97 03",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega1280",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 03"] = devices["ATmega1280"]
devices["ATmega1281"] = {["signature"] = "1e 97 04",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega1281",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 04"] = devices["ATmega1281"]
devices["ATmega1284"] = {["signature"] = "1e 97 06",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega1284",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 06"] = devices["ATmega1284"]
devices["ATmega1284P"] = {["signature"] = "1e 97 05",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega1284P",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 05"] = devices["ATmega1284P"]
devices["ATmega1284RFR2"] = {["signature"] = "1e a7 03",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega1284RFR2",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e a7 03"] = devices["ATmega1284RFR2"]
devices["ATmega128A"] = {["signature"] = "1e 97 02",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega128A",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e 97 02"] = devices["ATmega128A"]
devices["ATmega128RFA1"] = {["signature"] = "1e a7 01",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega128RFA1",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e a7 01"] = devices["ATmega128RFA1"]
devices["ATmega128RFR2"] = {["signature"] = "1e a7 02",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega128RFR2",["flash"] = {["pagesize"] = 256,["size"] = 131072}}
devices["1e a7 02"] = devices["ATmega128RFR2"]
devices["ATmega16"] = {["signature"] = "1e 94 03",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 03"] = devices["ATmega16"]
devices["ATmega162"] = {["signature"] = "1e 94 04",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega162",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 04"] = devices["ATmega162"]
devices["ATmega164A"] = {["signature"] = "1e 94 0f",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega164A",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0f"] = devices["ATmega164A"]
devices["ATmega164P"] = {["signature"] = "1e 94 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega164P",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0a"] = devices["ATmega164P"]
devices["ATmega164PA"] = {["signature"] = "1e 94 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega164PA",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0a"] = devices["ATmega164PA"]
devices["ATmega165A"] = {["signature"] = "1e 94 10",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega165A",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 10"] = devices["ATmega165A"]
devices["ATmega165P"] = {["signature"] = "1e 94 07",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega165P",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 07"] = devices["ATmega165P"]
devices["ATmega165PA"] = {["signature"] = "1e 94 07",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega165PA",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 07"] = devices["ATmega165PA"]
devices["ATmega168"] = {["signature"] = "1e 94 06",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega168",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 06"] = devices["ATmega168"]
devices["ATmega168A"] = {["signature"] = "1e 94 06",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega168A",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 06"] = devices["ATmega168A"]
devices["ATmega168P"] = {["signature"] = "1e 94 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega168P",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0b"] = devices["ATmega168P"]
devices["ATmega168PA"] = {["signature"] = "1e 94 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega168PA",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0b"] = devices["ATmega168PA"]
devices["ATmega168PB"] = {["signature"] = "1e 94 15",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega168PB",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 15"] = devices["ATmega168PB"]
devices["ATmega169A"] = {["signature"] = "1e 94 11",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega169A",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 11"] = devices["ATmega169A"]
devices["ATmega169P"] = {["signature"] = "1e 94 05",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega169P",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 05"] = devices["ATmega169P"]
devices["ATmega169PA"] = {["signature"] = "1e 94 05",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega169PA",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 05"] = devices["ATmega169PA"]
devices["ATmega16A"] = {["signature"] = "1e 94 03",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16A",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 03"] = devices["ATmega16A"]
devices["ATmega16HVA"] = {["signature"] = "1e 94 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega16HVA",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0c"] = devices["ATmega16HVA"]
devices["ATmega16HVB"] = {["signature"] = "1e 94 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16HVB",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0d"] = devices["ATmega16HVB"]
devices["ATmega16HVBrevB"] = {["signature"] = "1e 94 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16HVBrevB",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 0d"] = devices["ATmega16HVBrevB"]
devices["ATmega16M1"] = {["signature"] = "1e 94 84",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16M1",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 84"] = devices["ATmega16M1"]
devices["ATmega16U2"] = {["signature"] = "1e 94 89",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16U2",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 89"] = devices["ATmega16U2"]
devices["ATmega16U4"] = {["signature"] = "1e 94 88",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega16U4",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 88"] = devices["ATmega16U4"]
devices["ATmega2560"] = {["signature"] = "1e 98 01",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega2560",["flash"] = {["pagesize"] = 256,["size"] = 262144}}
devices["1e 98 01"] = devices["ATmega2560"]
devices["ATmega2561"] = {["signature"] = "1e 98 02",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega2561",["flash"] = {["pagesize"] = 256,["size"] = 262144}}
devices["1e 98 02"] = devices["ATmega2561"]
devices["ATmega2564RFR2"] = {["signature"] = "1e a8 03",["eeprom"] = {["pagesize"] = 8,["size"] = 8192},["name"] = "ATmega2564RFR2",["flash"] = {["pagesize"] = 256,["size"] = 262144}}
devices["1e a8 03"] = devices["ATmega2564RFR2"]
devices["ATmega256RFR2"] = {["signature"] = "1e a8 02",["eeprom"] = {["pagesize"] = 8,["size"] = 8192},["name"] = "ATmega256RFR2",["flash"] = {["pagesize"] = 256,["size"] = 262144}}
devices["1e a8 02"] = devices["ATmega256RFR2"]
devices["ATmega32"] = {["signature"] = "1e 95 02",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 02"] = devices["ATmega32"]
devices["ATmega324A"] = {["signature"] = "1e 95 15",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega324A",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 15"] = devices["ATmega324A"]
devices["ATmega324P"] = {["signature"] = "1e 95 08",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega324P",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 08"] = devices["ATmega324P"]
devices["ATmega324PA"] = {["signature"] = "1e 95 11",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega324PA",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 11"] = devices["ATmega324PA"]
devices["ATmega324PB"] = {["signature"] = "1e 95 17",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega324PB",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 17"] = devices["ATmega324PB"]
devices["ATmega325"] = {["signature"] = "1e 95 05",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega325",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 05"] = devices["ATmega325"]
devices["ATmega3250"] = {["signature"] = "1e 95 06",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3250",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 06"] = devices["ATmega3250"]
devices["ATmega3250A"] = {["signature"] = "1e 95 06",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3250A",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 06"] = devices["ATmega3250A"]
devices["ATmega3250P"] = {["signature"] = "1e 95 0e",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3250P",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0e"] = devices["ATmega3250P"]
devices["ATmega3250PA"] = {["signature"] = "1e 95 0e",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3250PA",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0e"] = devices["ATmega3250PA"]
devices["ATmega325A"] = {["signature"] = "1e 95 05",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega325A",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 05"] = devices["ATmega325A"]
devices["ATmega325P"] = {["signature"] = "1e 95 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega325P",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0d"] = devices["ATmega325P"]
devices["ATmega325PA"] = {["signature"] = "1e 95 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega325PA",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0d"] = devices["ATmega325PA"]
devices["ATmega328"] = {["signature"] = "1e 95 14",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega328",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 14"] = devices["ATmega328"]
devices["ATmega328P"] = {["signature"] = "1e 95 0f",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega328P",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0f"] = devices["ATmega328P"]
devices["ATmega328PB"] = {["signature"] = "1e 95 16",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega328PB",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 16"] = devices["ATmega328PB"]
devices["ATmega329"] = {["signature"] = "1e 95 03",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega329",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 03"] = devices["ATmega329"]
devices["ATmega3290"] = {["signature"] = "1e 95 04",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3290",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 04"] = devices["ATmega3290"]
devices["ATmega3290A"] = {["signature"] = "1e 95 04",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3290A",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 04"] = devices["ATmega3290A"]
devices["ATmega3290P"] = {["signature"] = "1e 95 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3290P",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0c"] = devices["ATmega3290P"]
devices["ATmega3290PA"] = {["signature"] = "1e 95 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega3290PA",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0c"] = devices["ATmega3290PA"]
devices["ATmega329A"] = {["signature"] = "1e 95 03",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega329A",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 03"] = devices["ATmega329A"]
devices["ATmega329P"] = {["signature"] = "1e 95 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega329P",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0b"] = devices["ATmega329P"]
devices["ATmega329PA"] = {["signature"] = "1e 95 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega329PA",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 0b"] = devices["ATmega329PA"]
devices["ATmega32A"] = {["signature"] = "1e 95 02",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32A",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 02"] = devices["ATmega32A"]
devices["ATmega32C1"] = {["signature"] = "1e 95 86",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32C1",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 86"] = devices["ATmega32C1"]
devices["ATmega32HVB"] = {["signature"] = "1e 95 10",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32HVB",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 10"] = devices["ATmega32HVB"]
devices["ATmega32HVBrevB"] = {["signature"] = "1e 95 10",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32HVBrevB",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 10"] = devices["ATmega32HVBrevB"]
devices["ATmega32M1"] = {["signature"] = "1e 95 84",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32M1",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 84"] = devices["ATmega32M1"]
devices["ATmega32U2"] = {["signature"] = "1e 95 8a",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32U2",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 8a"] = devices["ATmega32U2"]
devices["ATmega32U4"] = {["signature"] = "1e 95 87",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega32U4",["flash"] = {["pagesize"] = 128,["size"] = 32768}}
devices["1e 95 87"] = devices["ATmega32U4"]
devices["ATmega406"] = {["signature"] = "1e 95 07",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega406",["flash"] = {["pagesize"] = 128,["size"] = 40960}}
devices["1e 95 07"] = devices["ATmega406"]
devices["ATmega48"] = {["signature"] = "1e 92 05",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega48",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 05"] = devices["ATmega48"]
devices["ATmega48A"] = {["signature"] = "1e 92 05",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega48A",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 05"] = devices["ATmega48A"]
devices["ATmega48P"] = {["signature"] = "1e 92 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega48P",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 0a"] = devices["ATmega48P"]
devices["ATmega48PA"] = {["signature"] = "1e 92 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega48PA",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 0a"] = devices["ATmega48PA"]
devices["ATmega48PB"] = {["signature"] = "1e 92 10",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega48PB",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 10"] = devices["ATmega48PB"]
devices["ATmega64"] = {["signature"] = "1e 96 02",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega64",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 02"] = devices["ATmega64"]
devices["ATmega640"] = {["signature"] = "1e 96 08",["eeprom"] = {["pagesize"] = 8,["size"] = 4096},["name"] = "ATmega640",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 08"] = devices["ATmega640"]
devices["ATmega644"] = {["signature"] = "1e 96 09",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega644",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 09"] = devices["ATmega644"]
devices["ATmega644A"] = {["signature"] = "1e 96 09",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega644A",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 09"] = devices["ATmega644A"]
devices["ATmega644P"] = {["signature"] = "1e 96 0a",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega644P",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 0a"] = devices["ATmega644P"]
devices["ATmega644PA"] = {["signature"] = "1e 96 0a",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega644PA",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 0a"] = devices["ATmega644PA"]
devices["ATmega644RFR2"] = {["signature"] = "1e a6 03",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega644RFR2",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e a6 03"] = devices["ATmega644RFR2"]
devices["ATmega645"] = {["signature"] = "1e 96 05",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega645",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 05"] = devices["ATmega645"]
devices["ATmega6450"] = {["signature"] = "1e 96 06",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega6450",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 06"] = devices["ATmega6450"]
devices["ATmega6450A"] = {["signature"] = "1e 96 06",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega6450A",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 06"] = devices["ATmega6450A"]
devices["ATmega6450P"] = {["signature"] = "1e 96 0e",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega6450P",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 0e"] = devices["ATmega6450P"]
devices["ATmega645A"] = {["signature"] = "1e 96 05",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega645A",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 05"] = devices["ATmega645A"]
devices["ATmega645P"] = {["signature"] = "1e 96 0d",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega645P",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 0d"] = devices["ATmega645P"]
devices["ATmega649"] = {["signature"] = "1e 96 03",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega649",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 03"] = devices["ATmega649"]
devices["ATmega6490"] = {["signature"] = "1e 96 04",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega6490",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 04"] = devices["ATmega6490"]
devices["ATmega6490A"] = {["signature"] = "1e 96 04",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega6490A",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 04"] = devices["ATmega6490A"]
devices["ATmega6490P"] = {["signature"] = "1e 96 0c",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega6490P",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 0c"] = devices["ATmega6490P"]
devices["ATmega649A"] = {["signature"] = "1e 96 03",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega649A",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 03"] = devices["ATmega649A"]
devices["ATmega649P"] = {["signature"] = "1e 96 0b",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega649P",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 0b"] = devices["ATmega649P"]
devices["ATmega64A"] = {["signature"] = "1e 96 02",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega64A",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 02"] = devices["ATmega64A"]
devices["ATmega64C1"] = {["signature"] = "1e 96 86",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega64C1",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 86"] = devices["ATmega64C1"]
devices["ATmega64HVE2"] = {["signature"] = "1e 96 10",["eeprom"] = {["pagesize"] = 4,["size"] = 1024},["name"] = "ATmega64HVE2",["flash"] = {["pagesize"] = 128,["size"] = 65536}}
devices["1e 96 10"] = devices["ATmega64HVE2"]
devices["ATmega64M1"] = {["signature"] = "1e 96 84",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega64M1",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e 96 84"] = devices["ATmega64M1"]
devices["ATmega64RFR2"] = {["signature"] = "1e a6 02",["eeprom"] = {["pagesize"] = 8,["size"] = 2048},["name"] = "ATmega64RFR2",["flash"] = {["pagesize"] = 256,["size"] = 65536}}
devices["1e a6 02"] = devices["ATmega64RFR2"]
devices["ATmega8"] = {["signature"] = "1e 93 07",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega8",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 07"] = devices["ATmega8"]
devices["ATmega8515"] = {["signature"] = "1e 93 06",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega8515",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 06"] = devices["ATmega8515"]
devices["ATmega8535"] = {["signature"] = "1e 93 08",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega8535",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 08"] = devices["ATmega8535"]
devices["ATmega88"] = {["signature"] = "1e 93 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega88",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0a"] = devices["ATmega88"]
devices["ATmega88A"] = {["signature"] = "1e 93 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega88A",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0a"] = devices["ATmega88A"]
devices["ATmega88P"] = {["signature"] = "1e 93 0f",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega88P",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0f"] = devices["ATmega88P"]
devices["ATmega88PA"] = {["signature"] = "1e 93 0f",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega88PA",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0f"] = devices["ATmega88PA"]
devices["ATmega88PB"] = {["signature"] = "1e 93 16",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega88PB",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 16"] = devices["ATmega88PB"]
devices["ATmega8A"] = {["signature"] = "1e 93 07",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega8A",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 07"] = devices["ATmega8A"]
devices["ATmega8HVA"] = {["signature"] = "1e 93 10",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATmega8HVA",["flash"] = {["pagesize"] = 128,["size"] = 8192}}
devices["1e 93 10"] = devices["ATmega8HVA"]
devices["ATmega8U2"] = {["signature"] = "1e 93 89",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATmega8U2",["flash"] = {["pagesize"] = 128,["size"] = 8192}}
devices["1e 93 89"] = devices["ATmega8U2"]
devices["ATtiny10"] = {["signature"] = "1e 90 03",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny10",["flash"] = {["pagesize"] = 128,["size"] = 1024}}
devices["1e 90 03"] = devices["ATtiny10"]
devices["ATtiny102"] = {["signature"] = "1e 90 0c",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny102",["flash"] = {["pagesize"] = 128,["size"] = 1024}}
devices["1e 90 0c"] = devices["ATtiny102"]
devices["ATtiny104"] = {["signature"] = "1e 90 0b",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny104",["flash"] = {["pagesize"] = 128,["size"] = 1024}}
devices["1e 90 0b"] = devices["ATtiny104"]
devices["ATtiny11"] = {["signature"] = "1e 90 04",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny11",["flash"] = {["pagesize"] = 0,["size"] = 1024}}
devices["1e 90 04"] = devices["ATtiny11"]
devices["ATtiny12"] = {["signature"] = "1e 90 05",["eeprom"] = {["pagesize"] = 2,["size"] = 64},["name"] = "ATtiny12",["flash"] = {["pagesize"] = 0,["size"] = 1024}}
devices["1e 90 05"] = devices["ATtiny12"]
devices["ATtiny13"] = {["signature"] = "1e 90 07",["eeprom"] = {["pagesize"] = 4,["size"] = 64},["name"] = "ATtiny13",["flash"] = {["pagesize"] = 32,["size"] = 1024}}
devices["1e 90 07"] = devices["ATtiny13"]
devices["ATtiny13A"] = {["signature"] = "1e 90 07",["eeprom"] = {["pagesize"] = 4,["size"] = 64},["name"] = "ATtiny13A",["flash"] = {["pagesize"] = 32,["size"] = 1024}}
devices["1e 90 07"] = devices["ATtiny13A"]
devices["ATtiny15"] = {["signature"] = "1e 90 06",["eeprom"] = {["pagesize"] = 2,["size"] = 64},["name"] = "ATtiny15",["flash"] = {["pagesize"] = 0,["size"] = 1024}}
devices["1e 90 06"] = devices["ATtiny15"]
devices["ATtiny1634"] = {["signature"] = "1e 94 12",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny1634",["flash"] = {["pagesize"] = 32,["size"] = 16384}}
devices["1e 94 12"] = devices["ATtiny1634"]
devices["ATtiny167"] = {["signature"] = "1e 94 87",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny167",["flash"] = {["pagesize"] = 128,["size"] = 16384}}
devices["1e 94 87"] = devices["ATtiny167"]
devices["ATtiny20"] = {["signature"] = "1e 91 0f",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny20",["flash"] = {["pagesize"] = 128,["size"] = 2048}}
devices["1e 91 0f"] = devices["ATtiny20"]
devices["ATtiny2313"] = {["signature"] = "1e 91 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny2313",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 0a"] = devices["ATtiny2313"]
devices["ATtiny2313A"] = {["signature"] = "1e 91 0a",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny2313A",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 0a"] = devices["ATtiny2313A"]
devices["ATtiny24"] = {["signature"] = "1e 91 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny24",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 0b"] = devices["ATtiny24"]
devices["ATtiny24A"] = {["signature"] = "1e 91 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny24A",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 0b"] = devices["ATtiny24A"]
devices["ATtiny25"] = {["signature"] = "1e 91 08",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny25",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 08"] = devices["ATtiny25"]
devices["ATtiny26"] = {["signature"] = "1e 91 09",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny26",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 09"] = devices["ATtiny26"]
devices["ATtiny261"] = {["signature"] = "1e 91 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny261",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 0c"] = devices["ATtiny261"]
devices["ATtiny261A"] = {["signature"] = "1e 91 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 128},["name"] = "ATtiny261A",["flash"] = {["pagesize"] = 32,["size"] = 2048}}
devices["1e 91 0c"] = devices["ATtiny261A"]
devices["ATtiny4"] = {["signature"] = "1e 8f 0a",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny4",["flash"] = {["pagesize"] = 128,["size"] = 512}}
devices["1e 8f 0a"] = devices["ATtiny4"]
devices["ATtiny40"] = {["signature"] = "1e 92 0e",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny40",["flash"] = {["pagesize"] = 128,["size"] = 4096}}
devices["1e 92 0e"] = devices["ATtiny40"]
devices["ATtiny4313"] = {["signature"] = "1e 92 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny4313",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 0d"] = devices["ATtiny4313"]
devices["ATtiny43U"] = {["signature"] = "1e 92 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 64},["name"] = "ATtiny43U",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 0c"] = devices["ATtiny43U"]
devices["ATtiny44"] = {["signature"] = "1e 92 07",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny44",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 07"] = devices["ATtiny44"]
devices["ATtiny441"] = {["signature"] = "1e 92 15",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny441",["flash"] = {["pagesize"] = 16,["size"] = 4096}}
devices["1e 92 15"] = devices["ATtiny441"]
devices["ATtiny44A"] = {["signature"] = "1e 92 07",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny44A",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 07"] = devices["ATtiny44A"]
devices["ATtiny45"] = {["signature"] = "1e 92 06",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny45",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 06"] = devices["ATtiny45"]
devices["ATtiny461"] = {["signature"] = "1e 92 08",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny461",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 08"] = devices["ATtiny461"]
devices["ATtiny461A"] = {["signature"] = "1e 92 08",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny461A",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 08"] = devices["ATtiny461A"]
devices["ATtiny48"] = {["signature"] = "1e 92 09",["eeprom"] = {["pagesize"] = 4,["size"] = 64},["name"] = "ATtiny48",["flash"] = {["pagesize"] = 64,["size"] = 4096}}
devices["1e 92 09"] = devices["ATtiny48"]
devices["ATtiny5"] = {["signature"] = "1e 8f 09",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny5",["flash"] = {["pagesize"] = 128,["size"] = 512}}
devices["1e 8f 09"] = devices["ATtiny5"]
devices["ATtiny828"] = {["signature"] = "1e 93 14",["eeprom"] = {["pagesize"] = 4,["size"] = 256},["name"] = "ATtiny828",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 14"] = devices["ATtiny828"]
devices["ATtiny84"] = {["signature"] = "1e 93 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny84",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0c"] = devices["ATtiny84"]
devices["ATtiny841"] = {["signature"] = "1e 93 15",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny841",["flash"] = {["pagesize"] = 16,["size"] = 8192}}
devices["1e 93 15"] = devices["ATtiny841"]
devices["ATtiny84A"] = {["signature"] = "1e 93 0c",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny84A",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0c"] = devices["ATtiny84A"]
devices["ATtiny85"] = {["signature"] = "1e 93 0b",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny85",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0b"] = devices["ATtiny85"]
devices["ATtiny861"] = {["signature"] = "1e 93 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny861",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0d"] = devices["ATtiny861"]
devices["ATtiny861A"] = {["signature"] = "1e 93 0d",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny861A",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 0d"] = devices["ATtiny861A"]
devices["ATtiny87"] = {["signature"] = "1e 93 87",["eeprom"] = {["pagesize"] = 4,["size"] = 512},["name"] = "ATtiny87",["flash"] = {["pagesize"] = 128,["size"] = 8192}}
devices["1e 93 87"] = devices["ATtiny87"]
devices["ATtiny88"] = {["signature"] = "1e 93 11",["eeprom"] = {["pagesize"] = 4,["size"] = 64},["name"] = "ATtiny88",["flash"] = {["pagesize"] = 64,["size"] = 8192}}
devices["1e 93 11"] = devices["ATtiny88"]
devices["ATtiny9"] = {["signature"] = "1e 90 08",["eeprom"] = {["pagesize"] = 0,["size"] = 0},["name"] = "ATtiny9",["flash"] = {["pagesize"] = 128,["size"] = 1024}}
devices["1e 90 08"] = devices["ATtiny9"]
return devices
