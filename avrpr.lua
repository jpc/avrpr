#!/usr/bin/env thb
D = require 'util' D.prepend_timestamps = false D.prepend_thread_names = false

B = require 'binary'
T = require 'thread'
Object = require 'oo'

local H = B.hex2bin

local config = {
  product = 'SEPACK-NXP',
}

function Device (name, flash, flash_page, eeprom, eeprom_page)
  return {
    name = name,
    flash = flash * 1024,
    flash_page = flash_page * 2,
    eeprom = eeprom,
    eeprom_page = eeprom_page,
  }
end

local function cdbg (color, msg, ...)
  return D[color](msg)(...)
end
local function cdbgnn (color, msg, ...)
  return D[color](msg, true)(...)
end

function infonn (...)
  if options.verbose >= 1 then cdbgnn ('blue', ...) end
end
function info (...)
  if options.verbose >= 1 then cdbg ('blue', ...) end
end

signatures = require 'avr-devices'
signatures51 = require '8051-devices'

status = {}

commands = {}
function commands.connect ()
  gpio:seq():
    input'AVR_RESET':
    input'SCK':
    input'MOSI':
    input'MISO':
    run()
  gpio:seq():
    output'AVR_RESET':
    hi'AVR_RESET':
    peripheral('MISO', 'pull-none'):
    peripheral'MOSI':
    peripheral'SCK':
    run()
  info 'CONNECT'
end
function commands.disconnect ()
  gpio:seq():
    float'MISO':
    float'MOSI':
    float'SCK':
    hi'AVR_RESET':
    delay(10):
    float'AVR_RESET':
    run()
  info 'DISCONNECT'
end
function commands.reset ()
  gpio:seq():
    output'AVR_RESET':
    lo'AVR_RESET':
    delay(20):
    float'AVR_RESET':
    run()
end
function commands.programming_enable ()
  infonn 'PE '
  for i=1,10 do
    local seq = gpio:seq()
    if options.kind == '89S52' then
      seq:lo'AVR_RESET':
          delay(5):
          hi'AVR_RESET'
    else
      seq:hi'AVR_RESET':
          delay(5):
          lo'AVR_RESET'
    end
    seq:delay(20):
        read'AVR_RESET':
        run()
    infonn '.'
    result = spi:xchg(H'ac 53 00 00')
    if options.kind == '89S52' then
      if string.byte(result, 4) == 0x69 then info ' ok' return true end
    else
      if string.byte(result, 3) == 0x53 then info ' ok' return true end
    end
    info'' cdbg ('red', 'invalid programming enable reply:', D.hex(result))
  end
  error ("failed to synchronize with the device", 0)
end
function commands.read_signature ()
  local sig = {}
  if options.kind == '89S52' then
    for _,i in ipairs{0, 0x100, 0x200} do
      sig[#sig + 1] = string.format ('%02x', string.byte(spi:xchg(B.flat{0x28,B.enc16BE(i),0}), 4))
    end
  else
    for i=0,2 do
      sig[i + 1] = string.format ('%02x', string.byte(spi:xchg(B.flat{0x30,0x00,i,0}), 4))
    end
  end
  sig = table.concat (sig, ' ')
  info ('signature: ' .. sig)
  if options.kind == '89S52' then
    status.device = signatures51[sig]
  else
    status.device = signatures[sig]
  end
  D'kind:'(options.kind, status.device)
  return
end
do
  local function fmt(v)
    if not v then return "? kB" end
    return tostring (v / 1024) .. " kB"
  end
  function commands.display_info()
    local part = assert(status.device, "no valid device detected")
    D.green('Found '..part.name..' with '..fmt(part.flash.size) .. ' flash and '..fmt(part.eeprom.size)..' EEPROM')()
  end
end

function commands.chip_erase()
  spi:xchg(H'ac 80 00 00')
  if options.kind == '89S52' then
    T.sleep (1)
  else
    T.sleep (10/1000)
  end
end

function commands.read_flash_byte (addr)
  if options.kind == '89S52' then
    return string.byte(spi:xchg(B.flat{0x20, B.enc16BE(addr), 0}), 4)
  else
    local cmd
    if addr % 2 == 1 then cmd = 0x28 addr = addr - 1 else cmd = 0x20 end
    return string.byte(spi:xchg(cmd, addr / 512, (addr / 2) % 256, 0), 4)
  end
end
function commands.read_mem (mem, fmt, fname)
  if fmt ~= 'B' then error ('only raw binary file support is implemented', 0) end
  if mem == 'e' then
    error ('reading from EEPROM memory is not implemented', 0)
  elseif mem == 'f' then
    local bs = {}
    local pagesize = status.device.flash.pagesize
    for base=0,status.device.flash.size-1,pagesize do
      local code = {}
      for addr=base,base + pagesize - 1,2 do
        local a = addr / 2
        table.push (code, 0x20, a / 256, a % 256, 0)
        table.push (code, 0x28, a / 256, a % 256, 0)
      end
      local r = spi:xchg(B.flat(code))
      for i=4,#r,4 do
        bs[#bs+1] = string.char (r[i])
      end
    end
    local f = io.open (fname, "wb")
    if not f then error ("error: cannot open file for writing: " .. fname, 0) end
    f:write (table.concat (bs))
    f:close()
  end
end
function table.push (t, ...)
  for i=1,select('#', ...) do
    t[#t + 1] = select (i, ...)
  end
end
function commands.write_mem (mem, fmt, fname)
  if fmt ~= 'B' then error ("only raw binary file support is implemented", 0) end
  if mem == 'e' then
    error ("writing to EEPROM memory is not implemented", 0)
  elseif mem == 'f' then
    local f = io.open(fname, 'rb')
    if not f then error ("error: could not open file: " .. fname, 0) end
    local image = f:read('*all')
    f:close()
    local pagesize = status.device.flash.pagesize
    infonn'FLASH: '
    for base = 0,#image+(pagesize-1)-1,pagesize do
      local code = {}
      if options.kind == '89S52' then
        for addr = 0,pagesize - 1,1 do
          table.push (code, 0x40, B.enc16BE(base + addr), image:byte(base + addr + 1) or 0xff)
        end
      else
        for addr = 0,pagesize - 1,2 do
          local word_off = math.floor (addr / 2)
          table.push (code, 0x40, 0x00, word_off, image:byte(base + addr + 1) or 0xff)
          table.push (code, 0x48, 0x00, word_off, image:byte(base + addr + 2) or 0xff)
        end
      end
      spi:xchg(B.flat(code))
      if options.kind ~= '89S52' then
        spi:xchg(B.flat{0x4c, math.floor (base / 512), math.floor (base / 2) % 256, 0x00})
      end
      infonn '.'
      T.sleep (10/1000)
    end
    info' ok'
  end
end
function commands.verify_mem (mem, fmt, fname)
  if fmt ~= 'B' then error ('only raw binary file support is implemented', 0) end
  if mem == 'e' then
    error ('reading from EEPROM memory is not implemented', 0)
  elseif mem == 'f' then
    local f = io.open(fname, 'rb')
    if not f then error ("error: could not open file: " .. fname, 0) end
    local image = f:read('*all')
    f:close()
    local bs = {}
    if options.kind == '89S52' then
      for i=0,#image-1 do
        bs[#bs+1] = string.char(commands.read_flash_byte(i))
      end
    else
      local pagesize = status.device.flash.pagesize
      for base=0,status.device.flash.size-1,pagesize do
        local code = {}
        for addr=base,base + pagesize - 1,2 do
          local a = addr / 2
          table.push (code, 0x20, a / 256, a % 256, 0)
          table.push (code, 0x28, a / 256, a % 256, 0)
        end
        local r = spi:xchg(B.flat(code))
        for i=4,#r,4 do
          bs[#bs+1] = string.sub(r, i, i)
        end
      end
    end
    local readout = table.concat (bs)
    readout = string.sub(readout, 1, #image)
    if readout == image then
      info' ok'
    else
      error("verification failed: Flash")
    end
  end
end
do
  local kinds = {
    L = {name = "Lock bits", r = {0x58, 0x00, 0x00, 0}, w = {0xac, 0xe0, 0x00}},
    F = {name = "Fuse bits", r = {0x50, 0x00, 0x00, 0}, w = {0xac, 0xa0, 0x00}},
    H = {name = "High fuse bits", r = {0x58, 0x08, 0x00, 0}, w = {0xac, 0xa8, 0x00}},
    X = {name = "Extended fuse bits", r = {0x50, 0x08, 0x00, 0}, w = {0xac, 0xa4, 0x00}},
    C = {name = "Callibration byte", r = {0x50, 0x08, 0x00, 0}},
  }
  function commands.read_bits (kind)
    kind = kinds[kind]
    local r = string.byte(spi:xchg(B.flat(kind.r)), 4)
    cdbg ('green', kind.name .. ': ' .. string.format ("0x%02x", r))
    return r
  end
  function commands.write_bits (kind, v)
    kind = kinds[kind]
    if v:match ('^0x') then
      v = tonumber(v:sub(3), 16)
    else
      v = tonumber(v, 2)
    end
    b0, b1, b2 = unpack (kind.w)
    spi:xchg(B.flat{b0, b1, b2, v})
    T.sleep(.1)
    local r = string.byte(spi:xchg(B.flat(kind.r)), 4)
    if r ~= v then error("verification failed: "..kind.name) end
    T.sleep(.1)
  end
end

options = {
  verbose = 0,
  dry = false,
  discover = false,
  interactive = false,
  device = nil,
  sepack_serial = nil,
  kind = 'AVR',
  commands = {}
}

function parse_arguments ()
  local progname = arg[0]
  local o = options
  local function ensure_cmd (args)
    for i,cmd in ipairs(options.commands) do
      if cmd[1] == args[1] then return end
    end
    options.commands[#options.commands + 1] = args
  end
  local function add_cmd (args)
    options.commands[#options.commands + 1] = args
  end
  local function err (...)
    error (table.concat ({progname, ": error: ", ...}), 0)
  end
  local i = 1
  while i < #arg + 1 do
    local cmd = arg[i]
    local function next_arg()
      s = arg[i + 1]; i = i + 1
      if not s then
        err("argument required for ", cmd)
      end
      return s
    end
        if cmd == '-v' then o.verbose = o.verbose + 1
    elseif cmd == '-n' then o.dry = true
    elseif cmd == '-L' then o.discover = true
    elseif cmd == '-i' then o.interactive = true
    elseif cmd == '-s' then o.sepack_serial = next_arg()
    elseif cmd == '-p' then add_cmd {'display_info'}
    elseif cmd == '-F' then o.device = next_arg()
    elseif cmd == '-E' then add_cmd {'chip_erase'}
    elseif cmd == '-l' then o.customcode = next_arg()
    elseif cmd == '-89S52' then o.kind = '89S52'
    elseif cmd:match'%-w[fe][HSB]' then fname = next_arg()
                                        ensure_cmd {'chip_erase'}
                                        add_cmd {'write_mem', cmd:sub(3,3), cmd:sub(4,4), fname}
                                        add_cmd {'verify_mem', cmd:sub(3,3), cmd:sub(4,4), fname}
    elseif cmd:match'%-r[fe][HSB]' then add_cmd {'read_mem', cmd:sub(3,3), cmd:sub(4,4), next_arg()}
    elseif cmd:match'%-r[LFHXC]' then add_cmd {'read_bits', cmd:sub(3,3)}
    elseif cmd:match'%-w[LFHX]' then
      bits = next_arg()
      if not bits:match'^[01]+$' and not bits:match'^0x[0-9a-fA-F]+$' then
        err ("invalid argument to ", cmd, ": ", bits)
      end
      add_cmd {'write_bits', cmd:sub(3,3), bits}
    else
      err ("invalid argument: ", cmd)
    end
    i = i + 1
  end
end

function main ()
  if #options.commands > 0 then
    commands.connect()
    commands.programming_enable()
    commands.read_signature()
  end
  for i,cmd in ipairs (options.commands) do
    info ('executing', cmd)
    commands[cmd[1]](unpack(cmd, 2))
  end
  commands.disconnect()
  if options.interactive then
    if options.customcode then
      local code, err = loadfile(options.customcode)
      if not code then D.red'customcode error:'(D.unq(err)) os.exit(2) end
      repl.execute(code)
    end
    repl.start(0)
  else
    os.exit(0)
  end
end

local Sepack = require 'sepack'
local ExtProc = require'extproc'

local function open_device ()
  local extproc_log, sepack_log
  if options.verbose > 2 then
    sepack_log = log:sub'sepack'
    if options.verbose > 3 then extproc_log = sepack_log:sub'ext' end
  end
  s = Sepack:new(ExtProc:newUsb(config.product, options.sepack_serial, extproc_log), sepack_log)
  s.verbose = options.verbose - 1
  while true do
    local status = s.connected:recv()
    if status == true then
      break
    elseif status == false then
      local msg = {config.product}
      if options.sepack_serial then
        msg[#msg+1] = D.unq("with serial number")
        msg[#msg+1] = options.sepack_serial
      end
      msg[#msg+1] = D.unq('not found')
      D.red'÷'(unpack(msg))
    end
  end
  spi = s.channels.spi
  spi:setup_master(100e3, 8, 0, 0)
  gpio = s.channels.gpio
  local ok, err = T.pcall(main)
  if not ok then D.red'error:'(D.unq(err)) os.exit(2) end
end

repl = require'repl'

ok, err = T.spcall (parse_arguments)
if not ok then
  cdbg ('red', err)
  os.exit(1)
end

local thd = T.go(open_device)
require'loop'.run()
